package com.example.avancelabo6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.avancelabo6.model.Contacto

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        //P2-9Recuperar un Bundle
        val bundle:Bundle?= intent.extras
        //P2-10 para que no sea nulo
        bundle?.let {
            // P2-11 debo recibir un Contacto completo , de serializable lo convierta a Contacto
                val contacto = it.getSerializable("KEY_CONTACTO") as Contacto
            println("${contacto.nombre}")
            println("${contacto.cargo}")
            println("${contacto.correo}")
            println("${contacto.numero}")


        }

    }
}