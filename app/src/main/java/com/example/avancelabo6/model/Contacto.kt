package com.example.avancelabo6.model

import java.io.Serializable

data class Contacto (
    val nombre:String,
    val correo:String,
    val cargo:String,
    val numero:String //P2-8 se ve serializable
) : Serializable