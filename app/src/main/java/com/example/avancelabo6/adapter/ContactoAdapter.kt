package com.example.avancelabo6.adapter

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.avancelabo6.MainActivity
import com.example.avancelabo6.R
import com.example.avancelabo6.model.Contacto
import kotlinx.android.synthetic.main.item_contacto.view.*
import java.util.jar.Manifest


// 9.2 Recibe la lista de contactos //10.1 class ContactoAdapter(var contactos: MutableList<Contacto>):RecycleView.Adapter<clasepadre.clasehija>()
//10.2 class ContactoAdapter Alt enter E implmentamos los 3 metodos//P2-2 MutableList<Contacto>,val itemCallback : (item:Contacto)-> Unit)
class ContactoAdapter(var contactos: MutableList<Contacto>,
                      val contexto:Context,
                      val itemCallback : (item:Contacto)-> Unit):RecyclerView.Adapter<ContactoAdapter.ContactoAdapterViewHolder>() {

    //10  Crear clase interna(clase hija) ViewHolder, le mandamos el view
    class ContactoAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        //12 pinta la data , entonces tenemos que mandar el 11.3 aqui.
        fun bind(contacto: Contacto, contexto:Context) {
            itemView.textView2.text = contacto.nombre.get(0).toString()
            itemView.textView4.text = contacto.nombre
            itemView.textView5.text = contacto.cargo
            itemView.textView6.text = contacto.correo
          //P2-12 Clic en imagen, itemview es el xml, en el holder ya no el evento ONCLIC
            itemView.img1.setOnClickListener {
                println("Hola")
               // Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+Uri.encode(contacto.numero)))
             /**   val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:${contacto.numero}")**/
                 val intent = Intent(Intent.ACTION_DIAL);
                intent.data = Uri.parse("tel:${contacto.numero}")
                contexto.startActivity(intent)

            }
        }

    }
    
    //11 Implementar la clase adapter
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoAdapterViewHolder {
        //11.2 interactuar con mi plantilla con la que trabajaremos , el view viene ser el item_contacto.xml
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_contacto, parent, false)
        //le mando la vista a ContactoAdapterViewHolder para que setee mi informacion ya que tiene el xml y se insertara los datos en orden
        return ContactoAdapterViewHolder(view)

    }

    override fun getItemCount(): Int {
        //11.1 El me dice cuantos elementos tiene mi lista
        return contactos.size
    }

    override fun onBindViewHolder(holder: ContactoAdapterViewHolder, position: Int) {
        //11.3 Hace la cantidad de veces que se repetira mi lista
        val contacto = contactos[position] //1,2,3 (Obtengo la data)
        holder.bind(contacto,contexto) // 13. el val contacto lo mando al 11.2

        //P2-1
      //  holder.itemView.setOnClickListener {
          //  println("Hola")
            //Toast.makeText("",this,contacto.nombre,Toast.LENGTH_SHORT).show()
            //P-13 desactivar el evento OnClic
          //   holder.itemView.setOnClickListener {
           // itemCallback(contacto)
             //val intent= Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+Uri.encode(contacto.numero)))

         //   val intent = Intent(Intent.ACTION_CALL); intent.data = Uri.parse("tel:${contacto.numero}")
            }
        }
  //  }
//}
