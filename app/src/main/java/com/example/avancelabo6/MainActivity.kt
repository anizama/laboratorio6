package com.example.avancelabo6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.avancelabo6.adapter.ContactoAdapter
import com.example.avancelabo6.model.Contacto
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    //1. Creamos la lista:
    val contactos = mutableListOf<Contacto>()
    //4. variable adaptador
    lateinit var adaptador: ContactoAdapter// la variable la inicializara mas adelante
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

     //2. cargarinformacion ALT ENTER para crear funcion
        cargarinformacion()
     //5. configurar adaptador
        configuraradaptador()
    }

    //6. Funcion configurar adaptador
    private fun configuraradaptador() {
        //7. instanciar adaptador, llamarlo // 9. pasar la lista a la otra clase ContactoAdapter
        adaptador= ContactoAdapter(contactos,this) {
        //P2-3
            Toast.makeText(this,it.nombre,Toast.LENGTH_SHORT).show()//setea, si deseo hacer intent lo hago ahi
            //P2-5 Pero podemos pasar la informacion por un bundle
            val bundle=Bundle().apply {
                //P2-7
                putSerializable("KEY_CONTACTO",it) //alt enter para que sea el it serializable
              /**  putString("KEY NOMBRE",it.nombre)
                putString("KEY CORREO",it.correo)
                putString("KEY CARGO",it.cargo)
                putString("KEY NUMERO",it.numero)**/

            }
         //P2-4  PODRIA SERVIR PARA LLEGAR DE UN LISTADO AL DETALLE// P2-6  (this,DestinoActivity::class.java).apply
            val intent = Intent(this,DestinoActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }// 9.1 alt enter  agregamos parametro
        recycleviewcontactos.adapter=adaptador
        //8 propiedad layout Lineal o Grid
        recycleviewcontactos.layoutManager=LinearLayoutManager(this)
    }

    //3. Funcion cargarinformacion creada , antes creamos clase Contacto
    private fun cargarinformacion() {
        contactos.add(Contacto("Elvis","elvis@gmail.com","analista","123"))
        contactos.add(Contacto("Italo","italo@gmail.com","practicante","1234"))
        contactos.add(Contacto("Omar","omar@gmail.com","analista Sr","12345"))
    }

}